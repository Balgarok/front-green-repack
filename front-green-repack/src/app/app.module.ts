import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AchatComponent } from './components/article/achat/achat.component';
import { ViewArticleComponent } from './components/article/view/view.component';
import { CreateArticleExampleComponent } from './components/articleExample/create/create.component';
import { HomeComponent } from './components/home/home.component';
import { ListArticlesComponent } from './components/list-articles/list-articles.component';
import { ListCategoryComponent } from './components/list-category/list-category.component';
import { ListExampleComponent } from './components/list-example/list-example.component';
import { ListPromesseComponent } from './components/list-promesse/list-promesse.component';
import { LoginEmployeComponent } from './components/login-employe/login-employe.component';
import { FooterComponent } from './components/partials/footer/footer.component';
import { HeaderComponent } from './components/partials/header/header.component';
import { HeaderPageComponent } from './components/partials/header-page/header-page.component';
import { NotFoundComponent } from './components/partials/not-found/not-found.component';
import { EditPromesseComponent } from './components/promesse/edit/edit.component';
import { AccountPageComponent } from './components/users/account-page/account-page.component';
import { LoginComponent } from './components/users/login/login.component';
import { SignupComponent } from './components/users/signup/signup.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { ListPromesseUserComponent } from './components/list-promesse-user/list-promesse-user.component';
import { CreateCategoryComponent } from './components/category/create/create.component';
import { PromessePriseEnChargeComponent } from './components/promesse-prise-en-charge/promesse-prise-en-charge.component';
import { ListePromessePriseComponent } from './components/liste-promesse-prise/liste-promesse-prise.component';
import { CreatePromesseComponent } from './components/promesse/create/create.component';
import { PromesseVueClientComponent } from './components/promesse-vue-client/promesse-vue-client.component';
import { EditPromessePriseComponent } from './components/edit-promesse-prise/edit-promesse-prise.component';
import { AchatUserComponent } from './components/achat-user/achat-user.component';
import { ProduitMarchandComponent } from './components/produit-marchand/produit-marchand.component';
import { ListArticlesEmployeComponent } from './components/list-articles-employe/list-articles-employe.component';
import { DemandeAttenteMarchandComponent } from './components/demande-attente-marchand/demande-attente-marchand.component';

@NgModule({
  declarations: [
    AppComponent,
    AchatComponent,
    ViewArticleComponent,
    CreateArticleExampleComponent,
    HomeComponent,
    ListArticlesComponent,
    ListCategoryComponent,
    ListExampleComponent,
    ListPromesseComponent,
    LoginEmployeComponent,
    FooterComponent,
    HeaderComponent,
    HeaderPageComponent,
    NotFoundComponent,
    EditPromesseComponent,
    AccountPageComponent,
    LoginComponent,
    SignupComponent,
    ListPromesseUserComponent,
    CreateCategoryComponent,
    PromessePriseEnChargeComponent,
    ListePromessePriseComponent,
    CreatePromesseComponent,
    PromesseVueClientComponent,
    EditPromessePriseComponent,
    AchatUserComponent,
    ProduitMarchandComponent,
    ListArticlesEmployeComponent,
    DemandeAttenteMarchandComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
