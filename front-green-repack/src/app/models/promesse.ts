export class Promesse {
  _id: string
  marque: string
  modele: string
  prix: number
  image: string
  caracteriques_technique: string
  etat_esthetique: string
  vente: string
  etat: string
  userId: string
  employeId: string
  bon: string
  createdAt: Date
}
