export class Article {
  _id: string
  marque: string
  modele: string
  prix: number
  image: string
  caracteriques_technique: string
  etat_esthetique: string
  vendeur: string
  acheteur: string
  vendu: string
  createdAt: Date
}
