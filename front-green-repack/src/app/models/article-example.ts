export class ArticleExample {
  _id: string
  marque: string
  modele: string
  prix: number
  category: string
  createdAt: Date
}
