export class Clients {
  _id: string
  username: string
  firstname: string
  lastname: string
  email: string
  address: string
  marchand: string
  green_coin: number
}
