import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewArticleComponent implements OnInit {

  article: Article = new Article()
  userId: string
  paymentHandler:any = null;

  constructor(private articleService: ArticleService, private route: ActivatedRoute, private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    window.scrollTo(0,0)
    this.userId = this.auth.userId
    this.route.params.subscribe(
      (params: Params)=>{
        const id = params.id
        this.articleService.getArticleById(id)
        .then(
          (article: Article)=>{
            this.article = article
          }
        )
        .catch((err)=>{
          this.router.navigate(['/notFound'])
          console.log(err)
        })
      }
    )
  }

  onSubmit(){
    this.articleService.achatArticle(this.article._id, this.article, this.userId)
    .then(
      ()=>{
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        console.log(err.message);

      }
    )
  }

  retourList(){
    return this.router.navigate(['listArticles'])
  }

  initializePayment() {
    const paymentHandler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_51JgDB6KBq8n1Djk70RJxqh3fbc7Jaw6qUywnnH16Cq6hINr8HHLx0cyNKEh5TqJowxQRW2LzuVsBXEo0TwpANDTz00oSzyA1TA',
      locale: 'auto',
      token: function (stripeToken: any) {
        console.log({stripeToken})
        alert('Stripe token generated!');
      }
    });

    paymentHandler.open({
      name: this.article.marque +' '+this.article.modele,
      description: 'Buying a Hot Coffee',
      amount: this.article.prix * 100
    });
  }

  invokeStripe() {
    if(!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement("script");
      script.id = "stripe-script";
      script.type = "text/javascript";
      script.src = "https://checkout.stripe.com/checkout.js";
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: 'pk_test_51JgDB6KBq8n1Djk70RJxqh3fbc7Jaw6qUywnnH16Cq6hINr8HHLx0cyNKEh5TqJowxQRW2LzuVsBXEo0TwpANDTz00oSzyA1TA',
          locale: 'auto',
          token: function (stripeToken: any) {
            console.log(stripeToken)
            alert('Payment has been successfull!');
            this.onSubmit();
          }
        });
      }
      window.document.body.appendChild(script);
    }
  }
}
