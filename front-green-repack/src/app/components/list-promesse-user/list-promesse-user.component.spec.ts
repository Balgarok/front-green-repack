import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPromesseUserComponent } from './list-promesse-user.component';

describe('ListPromesseUserComponent', () => {
  let component: ListPromesseUserComponent;
  let fixture: ComponentFixture<ListPromesseUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPromesseUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPromesseUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
