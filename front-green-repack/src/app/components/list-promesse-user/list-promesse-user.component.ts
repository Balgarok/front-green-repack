import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-list-promesse-user',
  templateUrl: './list-promesse-user.component.html',
  styleUrls: ['./list-promesse-user.component.css']
})
export class ListPromesseUserComponent implements OnInit {

  promesses: Promesse[]
  promessesSub: Subscription
  userId: string

  constructor(private promesseService: PromesseService, private auth: AuthService) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.promessesSub = this.promesseService.promesses$.subscribe(
      (promesses: Promesse[])=>{
        this.promesses = promesses
      },
      (err)=>{
        console.log(err)
      }
    )
    this.promesseService.getPromesseUser(this.userId)
  }

  ngOnDestroy(): void{
    this.promessesSub.unsubscribe()
  }

}
