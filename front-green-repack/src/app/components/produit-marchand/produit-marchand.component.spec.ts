import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProduitMarchandComponent } from './produit-marchand.component';

describe('ProduitMarchandComponent', () => {
  let component: ProduitMarchandComponent;
  let fixture: ComponentFixture<ProduitMarchandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProduitMarchandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProduitMarchandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
