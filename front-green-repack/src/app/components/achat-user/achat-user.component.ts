import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-achat-user',
  templateUrl: './achat-user.component.html',
  styleUrls: ['./achat-user.component.css']
})
export class AchatUserComponent implements OnInit {

  articles: Article[]
  articlesSub: Subscription
  userId: string

  constructor(private articleService: ArticleService, private auth: AuthService) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.articlesSub = this.articleService.articles$.subscribe(
      (articles: Article[])=>{
        this.articles = articles
      },
      (err)=>{
        console.log(err)
      }
    )
    this.articleService.getArticleAchete(this.userId)
  }

  ngOnDestroy(): void{
    this.articlesSub.unsubscribe()
  }

}
