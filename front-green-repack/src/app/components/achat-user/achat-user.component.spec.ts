import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchatUserComponent } from './achat-user.component';

describe('AchatUserComponent', () => {
  let component: AchatUserComponent;
  let fixture: ComponentFixture<AchatUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AchatUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AchatUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
