import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-employe',
  templateUrl: './login-employe.component.html',
  styleUrls: ['./login-employe.component.css']
})
export class LoginEmployeComponent implements OnInit {

  loginForm!: FormGroup
  errorMessage :string

  constructor(private formBuilder: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      matricule: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  onSubmit(){
    const matriculeF = this.loginForm.get('matricule') as FormControl
    const passwordF = this.loginForm.get('password') as FormControl
    const matricule = matriculeF.value
    const password = passwordF.value
    this.auth.loginEm(matricule, password).then(
      ()=>{
        this.router.navigate(['/'])
      }
    ).catch(
      (err)=>{
        this.errorMessage = err.message
      }
    )
  }

}
