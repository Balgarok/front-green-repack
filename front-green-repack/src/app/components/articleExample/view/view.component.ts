import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ArticleExample } from 'src/app/models/article-example';
import { ArticleExampleService } from 'src/app/services/article-example.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewExampleComponent implements OnInit {

  example: ArticleExample = new ArticleExample()

  constructor(private exampleService: ArticleExampleService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    window.scrollTo(0,0)
    this.route.params.subscribe(
      (params: Params)=>{
        const id = params.id
        this.exampleService.getExampleById(id)
        .then(
          (example: ArticleExample)=>{
            this.example = example
          }
        )
        .catch((err)=>{
          this.router.navigate(['/notFound'])
          console.log(err)
        })
      }
    )
  }

  deleteExample(example: ArticleExample){
    this.exampleService.deleteExample(example._id)
    .then(
      ()=>{
        console.log("Category deleted")
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
  }

  retourList(){
    return this.router.navigate(['listCategory'])
  }

}
