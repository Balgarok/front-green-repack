import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ArticleExample } from 'src/app/models/article-example';
import { ArticleExampleService } from 'src/app/services/article-example.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateArticleExampleComponent implements OnInit {

  exampleForm: FormGroup = new FormGroup({})

  constructor(private formBuilder: FormBuilder, private exampleService: ArticleExampleService, private router: Router) { }

  ngOnInit(): void {
    this.exampleForm = this.formBuilder.group({
      marque: [null, Validators.required],
      modele: [null, Validators.required],
      prix: [null, Validators.required]
    })
  }

  onSubmit(){
    const example = new ArticleExample()
    example.marque = this.exampleForm.get('marque')?.value as string
    example.modele = this.exampleForm.get('modele')?.value as string
    example.prix = this.exampleForm.get('prix')?.value as number
    this.exampleService.createNewExample(example)
    .then(
      ()=>{
        this.exampleForm.reset()

        this.router.navigate(['/listArticleExample'])
      }
    )
    .catch(
      (err)=>{
      }
    )
  }

}
