import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewCategoryComponent implements OnInit {

  category: Category = new Category()

  constructor(private categoryService: CategoryService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    window.scrollTo(0,0)
    this.route.params.subscribe(
      (params: Params)=>{
        const id = params.id
        this.categoryService.getCategoryById(id)
        .then(
          (category: Category)=>{
            this.category = category
          }
        )
        .catch((err)=>{
          this.router.navigate(['/notFound'])
          console.log(err)
        })
      }
    )
  }

  deleteCategory(category: Category){
    this.categoryService.deleteCategory(category._id)
    .then(
      ()=>{
        console.log("Category deleted")
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
  }

  retourList(){
    return this.router.navigate(['listCategory'])
  }

}
