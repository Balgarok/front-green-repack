import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateCategoryComponent implements OnInit {

  categoryForm: FormGroup = new FormGroup({})

  constructor(private formBuilder: FormBuilder, private categoryService: CategoryService, private router: Router) { }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
      titre: [null, Validators.required],
      description: [null, Validators.required]
    })
  }

  onSubmit(){
    const category = new Category()
    category.titre = this.categoryForm.get('titre')?.value as string
    category.description = this.categoryForm.get('description')?.value as string
    this.categoryService.createNewCategory(category)
    .then(
      ()=>{
        this.categoryForm.reset()
        this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
      }
    )
  }

}
