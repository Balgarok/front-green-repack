import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {

  categories: Category[]
  categoriesSub: Subscription

  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.categoriesSub = this.categoryService.categories$.subscribe(
      (categories: Category[])=>{
        this.categories = categories
      },
      (err)=>{
        console.log(err)
      }
    )
    this.categoryService.getCategories()
  }

  ngOnDestroy(): void{
    this.categoriesSub.unsubscribe()
  }

}
