import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPromesseComponent } from './list-promesse.component';

describe('ListPromesseComponent', () => {
  let component: ListPromesseComponent;
  let fixture: ComponentFixture<ListPromesseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPromesseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPromesseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
