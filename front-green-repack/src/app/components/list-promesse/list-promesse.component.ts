import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-list-promesse',
  templateUrl: './list-promesse.component.html',
  styleUrls: ['./list-promesse.component.css']
})
export class ListPromesseComponent implements OnInit {

  promesses: Promesse[]
  promessesSub: Subscription
  userId: string

  constructor(private promesseService: PromesseService, private auth: AuthService) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.promessesSub = this.promesseService.promesses$.subscribe(
      (promesses: Promesse[])=>{
        this.promesses = promesses
      },
      (err)=>{
        console.log(err)
      }
    )
    this.promesseService.getPromesses()
  }

  ngOnDestroy(): void{
    this.promessesSub.unsubscribe()
  }

}
