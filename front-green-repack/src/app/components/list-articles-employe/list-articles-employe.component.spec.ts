import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListArticlesEmployeComponent } from './list-articles-employe.component';

describe('ListArticlesEmployeComponent', () => {
  let component: ListArticlesEmployeComponent;
  let fixture: ComponentFixture<ListArticlesEmployeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListArticlesEmployeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListArticlesEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
