import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean
  isClient: boolean
  isMarchand: boolean

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.isClient$.subscribe(
        (bool: boolean)=>{
          this.isClient = bool
        }
      )
    this.auth.isAuth$.subscribe(
      (bool: boolean)=>{
        this.isAuth = bool
      }
    )
    this.auth.isMarchand$.subscribe(
      (bool: boolean)=>{
        this.isMarchand = bool
      }
    )
  }

  logout(){
    this.auth.logout()
  }

}
