import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-edit-promesse-prise',
  templateUrl: './edit-promesse-prise.component.html',
  styleUrls: ['./edit-promesse-prise.component.css']
})
export class EditPromessePriseComponent implements OnInit {

  promesseForm: FormGroup
  userId: string
  promesse: Promesse = new Promesse()
  isLoaded: boolean = false
  isReception: boolean = false
  isContreOffre: boolean = false

  constructor(private formBuilder: FormBuilder, private promesseService: PromesseService, private router: Router, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.route.params.subscribe(
      (params: Params)=>{
        this.promesseService.getPromesseByIdEmp(params.id)
        .then(
          (promesse: Promesse)=>{
            this.promesse = promesse
            if(promesse.etat === 'Objet receptionne'){
              this.isReception = true
              this.promesseForm = this.formBuilder.group({
                marque: [{value: promesse.marque, disabled:true}, Validators.required],
                modele: [{value: promesse.modele, disabled:true}, Validators.required],
                carateristique_technique: [promesse.caracteriques_technique, Validators.required],
                etat_esthetique: [promesse.etat_esthetique, Validators.required],
                prix: [promesse.prix, Validators.required],
                etat: [{value: promesse.etat, disabled:true}, Validators.required],
              })
            }else{
              this.promesseForm = this.formBuilder.group({
                marque: [{value: promesse.marque, disabled:true}, Validators.required],
                modele: [{value: promesse.modele, disabled:true}, Validators.required],
                carateristique_technique: [promesse.caracteriques_technique, Validators.required],
                etat_esthetique: [promesse.etat_esthetique, Validators.required],
                prix: [{value: promesse.prix, disabled:true}, Validators.required],
                etat: [{value: promesse.etat, disabled:true}, Validators.required],
              })
            }
            if(promesse.etat === 'Contre offre client'){
              this.isContreOffre = true
            }
            this.isLoaded = true
          }
        )
        .catch(
          (err)=>{
            console.log(err.message)
            return this.router.navigate(['/'])
          }
        )
      }
    )
  }

  reception(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Objet receptionne'
    this.promesseService.updatePromesse(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  venteA(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Mise en vente acceptee'
    this.promesseService.updatePromesse(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  venteR(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Mise en vente refusee'
    this.promesseService.updatePromesse(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  contreO(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Contre offre'
    this.promesseService.updatePromesse(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  contreOffreR(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Contre offre Refuse'
    this.promesseService.refusEmploye(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  retourList(){
    return this.router.navigate(['listPromessesCharge'])
  }
}
