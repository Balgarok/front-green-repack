import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPromessePriseComponent } from './edit-promesse-prise.component';

describe('EditPromessePriseComponent', () => {
  let component: EditPromessePriseComponent;
  let fixture: ComponentFixture<EditPromessePriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPromessePriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPromessePriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
