import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromessePriseEnChargeComponent } from './promesse-prise-en-charge.component';

describe('PromessePriseEnChargeComponent', () => {
  let component: PromessePriseEnChargeComponent;
  let fixture: ComponentFixture<PromessePriseEnChargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromessePriseEnChargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromessePriseEnChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
