import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ArticleExample } from 'src/app/models/article-example';
import { ArticleExampleService } from 'src/app/services/article-example.service';

@Component({
  selector: 'app-list-example',
  templateUrl: './list-example.component.html',
  styleUrls: ['./list-example.component.css']
})
export class ListExampleComponent implements OnInit {

  examples: ArticleExample[]
  examplesSub: Subscription

  constructor(private exampleService: ArticleExampleService) { }

  ngOnInit(): void {
    this.examplesSub = this.exampleService.examples$.subscribe(
      (examples: ArticleExample[])=>{
        this.examples = examples
      },
      (err)=>{
        console.log(err)
      }
    )
    this.exampleService.getExamples()
  }

  ngOnDestroy(): void{
    this.examplesSub.unsubscribe()
  }

}
