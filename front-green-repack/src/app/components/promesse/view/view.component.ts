import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Promesse } from 'src/app/models/promesse';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewPromesseComponent implements OnInit {

  promesse: Promesse = new Promesse()

  constructor(private promesseService: PromesseService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    window.scrollTo(0,0)
    this.route.params.subscribe(
      (params: Params)=>{
        const id = params.id
        this.promesseService.getPromesseById(id)
        .then(
          (promesse: Promesse)=>{
            this.promesse = promesse
          }
        )
        .catch((err)=>{
          this.router.navigate(['/notFound'])
          console.log(err)
        })
      }
    )
  }

  deletePromesse(promesse: Promesse){
    this.promesseService.deletePromesse(promesse._id)
    .then(
      ()=>{
        console.log("Promesse deleted")
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
  }

}
