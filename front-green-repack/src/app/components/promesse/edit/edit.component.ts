import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditPromesseComponent implements OnInit {

  promesseForm: FormGroup
  userId: string
  promesse: Promesse = new Promesse()
  isLoaded: boolean = false

  constructor(private formBuilder: FormBuilder, private promesseService: PromesseService, private router: Router, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.route.params.subscribe(
      (params: Params)=>{
        this.promesseService.getPromesseByIdEmp(params.id)
        .then(
          (promesse: Promesse)=>{
            this.promesse = promesse
            this.isLoaded = true
            this.promesseForm = this.formBuilder.group({
              marque: [{value: promesse.marque, disabled:true}, Validators.required],
              modele: [{value: promesse.modele, disabled:true}, Validators.required],
              carateristique_technique: [{value: promesse.caracteriques_technique, disabled: true}, Validators.required],
              etat_esthetique: [{value: promesse.etat_esthetique, disabled: true}, Validators.required],
              etat: [{value: promesse.etat, disabled:true}, Validators.required],
            })
            return
          }
        )
        .catch(
          (err)=>{
            console.log(err.message)
            return this.router.navigate(['/'])
          }
        )
      }
    )
  }

  onSubmit(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.etat = this.promesseForm.get('etat')?.value as string
    this.promesseService.updatePromesse(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  retourList(){
    return this.router.navigate(['listPromessesCharge'])
  }

}
