import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { CategoryService } from 'src/app/services/category.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreatePromesseComponent implements OnInit {

  promesseForm: FormGroup = new FormGroup({})
  errorMessage: string = null
  userId: string

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private promesseService: PromesseService, private router: Router, private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.promesseForm = this.formBuilder.group({
      marque: [null, Validators.required],
      modele: [null, Validators.required],
      carateristique_technique: [null, Validators.required],
      etat_esthetique: [null, Validators.required],
      category: [null, Validators.required]
    })
    this.userId = this.auth.userId
  }

  onSubmit(){
    const promesse = new Promesse()
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.userId = this.userId
    this.promesseService.createNewPromesse(promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        this.router.navigate(['listPromessesUser'])
      }
    )
    .catch(
      (err)=>{
        this.errorMessage = err.message
      }
    )
  }

}
