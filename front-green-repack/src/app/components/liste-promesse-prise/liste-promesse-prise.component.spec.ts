import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListePromessePriseComponent } from './liste-promesse-prise.component';

describe('ListePromessePriseComponent', () => {
  let component: ListePromessePriseComponent;
  let fixture: ComponentFixture<ListePromessePriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListePromessePriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListePromessePriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
