import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-liste-promesse-prise',
  templateUrl: './liste-promesse-prise.component.html',
  styleUrls: ['./liste-promesse-prise.component.css']
})
export class ListePromessePriseComponent implements OnInit {

  promesses: Promesse[]
  promessesSub: Subscription
  userId: string
  isLoaded: boolean = false

  constructor(private promesseService: PromesseService, private auth: AuthService) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.promessesSub = this.promesseService.promesses$.subscribe(
      (promesses: Promesse[])=>{
        this.promesses = promesses
        this.isLoaded = true
      },
      (err)=>{
        console.log(err)
      }
    )
    this.promesseService.getPromesseByIdEmploye(this.userId as string)
  }

  ngOnDestroy(): void{
    this.promessesSub.unsubscribe()
  }
}
