import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.css']
})
export class ListArticlesComponent implements OnInit {

  articles: Article[]
  articlesSub: Subscription
  userId: string

  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
    this.articlesSub = this.articleService.articles$.subscribe(
      (articles: Article[])=>{
        this.articles = articles
      },
      (err)=>{
        console.log(err)
      }
    )
    this.articleService.getArticles()
  }

  ngOnDestroy(): void{
    this.articlesSub.unsubscribe()
  }

}
