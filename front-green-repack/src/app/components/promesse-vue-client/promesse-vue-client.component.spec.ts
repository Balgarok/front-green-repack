import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromesseVueClientComponent } from './promesse-vue-client.component';

describe('PromesseVueClientComponent', () => {
  let component: PromesseVueClientComponent;
  let fixture: ComponentFixture<PromesseVueClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromesseVueClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromesseVueClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
