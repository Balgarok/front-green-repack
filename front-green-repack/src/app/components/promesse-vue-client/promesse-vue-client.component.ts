import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Promesse } from 'src/app/models/promesse';
import { AuthService } from 'src/app/services/auth.service';
import { PromesseService } from 'src/app/services/promesse.service';

@Component({
  selector: 'app-promesse-vue-client',
  templateUrl: './promesse-vue-client.component.html',
  styleUrls: ['./promesse-vue-client.component.css']
})
export class PromesseVueClientComponent implements OnInit {

  promesseForm: FormGroup = new FormGroup({})
  userId: string
  promesse: Promesse = new Promesse()
  isLoaded: boolean = false
  isBon: boolean = false

  constructor(private formBuilder: FormBuilder, private promesseService: PromesseService, private router: Router, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.route.params.subscribe(
      (params: Params)=>{
        this.promesseService.getPromesseByIdUser(params.id)
        .then(
          (promesse: Promesse)=>{
            this.promesse = promesse
            if(promesse.bon!==undefined){
              this.isBon = true
              this.promesseForm = this.formBuilder.group({
                marque: [{value: this.promesse.marque, disabled:true}, Validators.required],
                modele: [{value: this.promesse.modele, disabled:true}, Validators.required],
                carateristique_technique: [{value: this.promesse.caracteriques_technique, disabled: true}, Validators.required],
                etat_esthetique: [{value: this.promesse.etat_esthetique, disabled: true}, Validators.required],
                prix:[{value: this.promesse.prix, disabled: true}, Validators.required],
                etat: [{value: this.promesse.etat, disabled:true}, Validators.required],
                bon: [{value:this.promesse.bon, disabled:true}, Validators.required]
              })
            }else{
              this.promesseForm = this.formBuilder.group({
                marque: [{value: this.promesse.marque, disabled:true}, Validators.required],
                modele: [{value: this.promesse.modele, disabled:true}, Validators.required],
                carateristique_technique: [{value: this.promesse.caracteriques_technique, disabled: true}, Validators.required],
                etat_esthetique: [{value: this.promesse.etat_esthetique, disabled: true}, Validators.required],
                prix:[this.promesse.prix, Validators.required],
                etat: [{value: this.promesse.etat, disabled:true}, Validators.required],
                bon: [{value:'', disabled:true}, Validators.required]
              })
            }
            this.isLoaded = true
          }
        )
        .catch(
          (err)=>{
            console.log(err.message)
            return this.router.navigate(['/'])
          }
        )
      }
    )
  }

  offre(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.etat = this.promesseForm.get('etat')?.value as string
    this.promesseService.offreAccepteByVendeur(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  accepter(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.etat = 'mise en vente'
    this.promesseService.miseEnVente(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  refuser(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    this.promesseService.deletePromesse(promesse._id)
  }

  contreO(){
    const promesse = new Promesse()
    promesse._id = this.promesse._id
    promesse.marque = this.promesseForm.get('marque')?.value as string
    promesse.modele = this.promesseForm.get('modele')?.value as string
    promesse.caracteriques_technique = this.promesseForm.get('carateristique_technique')?.value as string
    promesse.etat_esthetique = this.promesseForm.get('etat_esthetique')?.value as string
    promesse.prix = this.promesseForm.get('prix')?.value as number
    promesse.etat = 'Contre offre client'
    this.promesseService.miseEnVente(promesse._id, promesse)
    .then(
      ()=>{
        this.promesseForm.reset()
        return this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        return this.router.navigate(['/'])
      }
    )
    return
  }

  retourList(){
    return this.router.navigate(['listPromessesUser'])
  }
}
