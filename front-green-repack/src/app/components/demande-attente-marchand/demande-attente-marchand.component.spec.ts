import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAttenteMarchandComponent } from './demande-attente-marchand.component';

describe('DemandeAttenteMarchandComponent', () => {
  let component: DemandeAttenteMarchandComponent;
  let fixture: ComponentFixture<DemandeAttenteMarchandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandeAttenteMarchandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandeAttenteMarchandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
