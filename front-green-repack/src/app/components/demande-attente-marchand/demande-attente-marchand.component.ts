import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Clients } from 'src/app/models/clients';
import { AuthService } from 'src/app/services/auth.service';
import { ClientsService } from 'src/app/services/clients.service';

@Component({
  selector: 'app-demande-attente-marchand',
  templateUrl: './demande-attente-marchand.component.html',
  styleUrls: ['./demande-attente-marchand.component.css']
})
export class DemandeAttenteMarchandComponent implements OnInit {

  clients: Clients[]
  clientsSub: Subscription
  userId: string

  constructor(private clientService : ClientsService, private auth: AuthService) { }

  ngOnInit(): void {
    this.clientsSub = this.clientService.clients$.subscribe(
      (clients: Clients[])=>{
        this.clients = clients
      },
      (err)=>{
        console.log(err)
      }
    )
    this.clientService.getDemande()
  }

  ngOnDestroy(): void{
    this.clientsSub.unsubscribe()
  }

  accepterDemande(id : string){
    this.clientService.accepterDemande(id)
  }
}
