import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm!: FormGroup
  errorMessage: string

  constructor(private formBuilder: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      firstname: [null, [Validators.required]],
      lastname: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      address: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  onSubmit(){
    const usernameF = this.signupForm.get('username') as FormControl
    const firstnameF = this.signupForm.get('firstname') as FormControl
    const lastnameF = this.signupForm.get('lastname') as FormControl
    const emailF = this.signupForm.get('email') as FormControl
    const addressF = this.signupForm.get('address') as FormControl
    const passwordF = this.signupForm.get('password') as FormControl
    const username = usernameF.value
    const firstname = firstnameF.value
    const lastname = lastnameF.value
    const email = emailF.value
    const address = addressF.value
    const password = passwordF.value
    this.auth.signup(email, password, firstname, lastname, username, address)
    .then(
      ()=>{
        this.router.navigate(['/'])
      }
    )
    .catch(
      (err)=>{
        this.errorMessage = err.message
      }
    )
  }

}
