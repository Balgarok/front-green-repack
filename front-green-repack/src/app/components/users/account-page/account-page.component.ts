import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Clients } from 'src/app/models/clients';
import { AuthService } from 'src/app/services/auth.service';
import { ClientsService } from 'src/app/services/clients.service';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css']
})
export class AccountPageComponent implements OnInit {

  user: Clients = new Clients()
  userForm: FormGroup
  isLoaded: boolean = false
  userId: string
  errorMessage: string
  isMarchand: boolean = false

  constructor(private formBuilder: FormBuilder, private clientService: ClientsService, private router: Router, private auth: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = this.auth.userId
    this.route.params.subscribe(
      (params: Params)=>{
        this.clientService.getClientById(this.userId)
        .then(
          (client: Clients)=>{
            this.user = client
            this.isLoaded = true
            if(client.marchand == 'oui'){
              this.isMarchand = true
            }
            this.userForm = this.formBuilder.group({
              firstname: [client.firstname, Validators.required],
              lastname: [client.lastname, Validators.required],
              address: [client.address, Validators.required]
            })
          }
        )
        .catch(
          (err)=>{
            console.log(err.message)
            return this.router.navigate(['/accountPage'])
          }
        )
      }
    )
  }

  onSubmit(){
    const client = new Clients()
    client._id = this.user._id
    client.firstname = this.userForm.get('firstname')?.value as string
    client.lastname = this.userForm.get('lastname')?.value as string
    client.address = this.userForm.get('address')?.value as string
    this.clientService.updateClient(this.user)
    .then(
      ()=>{
        this.userForm.reset()
        return this.router.navigate(['/accountPage'])
      }
    )
    .catch(
      (err)=>{
        this.errorMessage = err.message
        return this.router.navigate(['/accountPage'])
      }
    )
  }

  devenirMarchand(){
    this.clientService.devenirMarchand(this.user._id)
    .then(
      ()=>{
        this.userForm.reset()
        return this.router.navigate(['/accountPage'])
      }
    )
    .catch(
      (err)=>{
        this.errorMessage = err.message
        return this.router.navigate(['/accountPage'])
      }
    )
  }

}
