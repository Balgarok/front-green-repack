import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api = environment.api
  token!: string
  userId!: string
  typeUtilsateur: string
  isAuth$ = new BehaviorSubject<boolean>(false)
  isClient$ = new BehaviorSubject<boolean>(false)
  isMarchand$ = new BehaviorSubject<boolean>(false)

  constructor(private http: HttpClient, private router: Router) {
    this.initAuth()
  }

  initAuth(){
    if(typeof localStorage !== "undefined"){
      const data = JSON.parse(localStorage.getItem('auth')as string)
      if(data!==null){
          this.userId = data.userId
          this.token = data.token
          this.typeUtilsateur = data.type
          if(this.typeUtilsateur === 'client'){
            this.isClient$.next(true)
          }
          this.isAuth$.next(true)

      }
    }
  }

  signup(email: string, password: string, firstname: string, lastname: string, username: string, address: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/utilisateurs/signup', {email: email, password: password, firstname: firstname, lastname: lastname, username : username, address :address}).subscribe(
        (signupData: {status: number, message: string})=>{
          if(signupData.status === 201){
            // authentifier l'utilisateur
            this.login(username, password)
            .then(()=>{
              resolve(true)
            })
            .catch((err)=>{
              reject(err)
            })
          }
          else{
            reject(signupData.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  login(username: string, password: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/utilisateurs/login', {username: username, password: password}).subscribe(
        (authData: {userId: string, token: string, type: string})=>{
          this.token = authData.token
          this.userId = authData.userId
          this.typeUtilsateur = authData.type
          this.isAuth$.next(true)
          this.isClient$.next(true)
          if(authData.type=='marchand')
            this.isMarchand$.next(true)
          // save authData in local
          if(typeof localStorage !== 'undefined'){
            localStorage.setItem('auth', JSON.stringify(authData))
          }
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  loginEm(matricule: string, password: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/utilisateurs/loginEm', {matricule: matricule, password: password}).subscribe(
        (authData: {userId: string, token: string, type: string})=>{
          this.token = authData.token
          this.userId = authData.userId
          this.typeUtilsateur = authData.type
          this.isAuth$.next(true)
          // save authData in local
          if(typeof localStorage !== 'undefined'){
            localStorage.setItem('auth', JSON.stringify(authData))
          }
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  logout(){
    this.isAuth$.next(false)
    this.isClient$.next(false)
    this.userId = null
    this.token = null
    this.typeUtilsateur = null
    if(typeof localStorage !== 'undefined'){
      localStorage.removeItem('auth')
    }
    return this.router.navigate(['/'])
  }
}
