import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../models/category';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  api = environment.api
  categories: Category[]
  categories$ = new Subject<Category[]>()

  constructor(private http: HttpClient) { }

  getCategories(){
    this.http.get<any>(this.api+'/category/liste').subscribe(
      (data: Data)=>{
        if (data.status === 200){
          this.categories = data.result
          this.emitCategories()
        }
        else{
          console.log(data)
        }
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  getCategoriesCrea(){
    this.http.get<any>(this.api+'/category/listeCrea').subscribe(
      (data: Data)=>{
        if (data.status === 200){
          this.categories = data.result
          this.emitCategories()
        }
        else{
          console.log(data)
        }
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  emitCategories(){
    this.categories$.next(this.categories)
  }

  getCategoryById(id: string): Promise<Category>{
    return new Promise((resole, reject)=>{
      this.http.get<any>(this.api+'/category/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resole(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  createNewCategory(category: Category){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/category/', {titre: category.titre, description: category.description}).subscribe(
        (data: Data)=>{
          if(data.status === 201){
            this.getCategories()
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  deleteCategory(id: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/category/'+id).subscribe(
        ()=>{
          this.getCategories()
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
