import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Article } from '../models/article';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  api = environment.api
  articles: Article[]
  articles$ = new Subject<Article[]>()

  constructor(private http: HttpClient) { }

  getArticles() {
    this.http.get<any>(this.api + '/article/').subscribe(
      (data: Data) => {
        if (data.status === 200) {
          this.articles = data.result
          this.emitArticles()
        }
        else {
          console.log(data)
        }
      },
      (err) => {
        console.log(err)
      }
    )
  }

  emitArticles() {
    this.articles$.next(this.articles)
  }

  getArticleById(id: string): Promise<Article> {
    return new Promise((resole, reject) => {
      this.http.get<any>(this.api + '/article/' + id).subscribe(
        (data: Data) => {
          if (data.status === 200) {
            resole(data.result)
          }
          else {
            reject(data.message)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  achatArticle(id: string, article: Article, idAcheteur: string) {
    return new Promise((resolve, reject) => {
      this.http.put<any>(this.api + '/article/' + id, {marque: article.marque, modele: article.modele, prix: article.prix, vente: 'oui', userId: idAcheteur}).subscribe(
        (data: Data) => {
          if (data.status === 200) {
            resolve(data)
          }
          else {
            reject(data)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  getArticleAchete(id: string) {
    this.http.get<any>(this.api + '/article/userlist/'+id).subscribe(
      (data: Data) => {
        if (data.status === 200) {
          this.articles = data.result
          this.emitArticles()
        }
        else {
          console.log(data)
        }
      },
      (err) => {
        console.log(err)
      }
    )
  }

  getArticleMisEnVente(id: string) {
    this.http.get<any>(this.api + '/article/ventes/'+id).subscribe(
      (data: Data) => {
        if (data.status === 200) {
          this.articles = data.result
          this.emitArticles()
        }
        else {
          console.log(data)
        }
      },
      (err) => {
        console.log(err)
      }
    )
  }

  deleteArticle(id: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/article/'+id).subscribe(
        ()=>{
          this.getArticles()
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
