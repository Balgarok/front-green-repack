import { TestBed } from '@angular/core/testing';

import { PromesseService } from './promesse.service';

describe('PromesseService', () => {
  let service: PromesseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PromesseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
