import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ArticleExample } from '../models/article-example';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class ArticleExampleService {

  api = environment.api
  examples: ArticleExample[]
  examples$ = new Subject<ArticleExample[]>()

  constructor(private http: HttpClient) { }

  getExamples(){
    this.http.get<any>(this.api+'/example/liste').subscribe(
      (data: Data)=>{
        if (data.status === 200){
          this.examples = data.result
          this.emitExamples()
        }
        else{
          console.log(data)
        }
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  emitExamples(){
    this.examples$.next(this.examples)
  }

  getExampleById(id: string): Promise<ArticleExample>{
    return new Promise((resole, reject)=>{
      this.http.get<any>(this.api+'/example/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resole(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  createNewExample(example: ArticleExample){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/example/', {marque: example.marque, modele:example.modele, prix: example.prix}).subscribe(
        (data: Data)=>{
          if(data.status === 201){
            this.getExamples()
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  updateExample(id: string, example: ArticleExample){
    return new Promise((resolve, reject)=>{
      let exampleData: FormData = new FormData()
      exampleData.append('example', JSON.stringify(example))
      this.http.put<any>(this.api+'/example/'+id, exampleData).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  deleteExample(id: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/example/'+id).subscribe(
        ()=>{
          this.getExamples()
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
