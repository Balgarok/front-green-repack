import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Clients } from '../models/clients';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  api = environment.api
  client: Clients
  clients: Clients[]
  client$= new Subject<Clients>()
  clients$ = new Subject<Clients[]>()

  constructor(private http: HttpClient) { }

  getClientById(id: string): Promise<Clients>{
    return new Promise((resole, reject) => {
      this.http.get<any>(this.api + '/utilisateurs/infos/' + id).subscribe(
        (data: Data) => {
          if (data.status === 200) {
            resole(data.result)
          }
          else {
            reject(data.message)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  updateClient(client: Clients){
    return new Promise((resolve, reject)=>{
      let clientData: FormData = new FormData()
      clientData.append('client', JSON.stringify(client))
      this.http.put<any>(this.api+'/utilisateurs/update', clientData).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  devenirMarchand(id: string){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/utilisateurs/marchand/'+id, {}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getDemande(): Promise<Clients[]>{
    return new Promise((resole, reject) => {
      this.http.get<any>(this.api + '/utilisateurs/demandeMarchand/').subscribe(
        (data: Data) => {
          if (data.status === 200) {
            this.clients = data.result
            this.emitClients()
          }
          else {
            reject(data.message)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  emitClients(){
    this.clients$.next(this.clients)
  }

  accepterDemande(id: string){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/utilisateurs/marchandOk'+id, {}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
