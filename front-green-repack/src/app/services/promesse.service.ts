import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Data } from '../models/data';
import { Promesse } from '../models/promesse';

@Injectable({
  providedIn: 'root'
})
export class PromesseService {

  api = environment.api
  promesses: Promesse[]
  promesses$ = new Subject<Promesse[]>()

  constructor(private http: HttpClient) { }

  getPromesses(){
    this.http.get<any>(this.api+'/promesse/').subscribe(
      (data: Data)=>{
        if (data.status === 200){
          this.promesses = data.result
          this.emitPromesses()
        }
        else{
          console.log(data)
        }
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  emitPromesses(){
    this.promesses$.next(this.promesses)
  }

  getPromesseById(id: string): Promise<Promesse>{
    return new Promise((resole, reject)=>{
      this.http.get<any>(this.api+'/promesse/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resole(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getPromesseByIdEmp(id: string): Promise<Promesse>{
    return new Promise((resole, reject)=>{
      this.http.get<any>(this.api+'/promesse/employeVue/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resole(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getPromesseByIdUser(id: string): Promise<Promesse>{
    return new Promise((resole, reject)=>{
      this.http.get<any>(this.api+'/promesse/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resole(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getPromesseByIdEmploye(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/promesse/listesuivi/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.promesses = data.result
            this.emitPromesses()
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  offreAccepteByVendeur(id: string, promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/offre/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristique_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  miseEnVente(id: string, promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/accepte/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristique_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, etat: promesse.etat}).subscribe(
        (data: Data)=>{
          if(data.status === 201){
            this.getPromesses()
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  createNewPromesse(promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/promesse/', {marque: promesse.marque, modele: promesse.modele, caracteristique_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, userId: promesse.userId}).subscribe(
        (data: Data)=>{
          if(data.status === 201){
            this.getPromesses()
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  updatePromesse(id: string, promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristique_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, prix: promesse.prix, etat: promesse.etat}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  deletePromesse(id: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/promesse/'+id).subscribe(
        ()=>{
          this.getPromesses()
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  refusEmploye(id: string, promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristique_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, prix: promesse.prix, etat: promesse.etat}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getPromesseUser(id: string){
    this.http.get<any>(this.api+'/promesse/userlist/'+id).subscribe(
      (data: Data)=>{
        if (data.status === 200){
          this.promesses = data.result
          this.emitPromesses()
        }
        else{
          console.log(data)
        }
      },
      (err)=>{
        console.log(err)
      }
    )
  }

  priseEnChargePromesse(id: string, promesse: Promesse, idEmp: string){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/charge/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristiques_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, etat: promesse.etat, employe:idEmp}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  contreOffre(id: string, promesse: Promesse){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/promesse/charge/'+id, {marque: promesse.marque, modele: promesse.modele, caracteristiques_technique: promesse.caracteriques_technique, etat_esthetique: promesse.etat_esthetique, etat: promesse.etat, prix: promesse.prix}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
