import { TestBed } from '@angular/core/testing';

import { ArticleExampleService } from './article-example.service';

describe('ArticleExampleService', () => {
  let service: ArticleExampleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticleExampleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
