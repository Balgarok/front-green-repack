import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AchatUserComponent } from './components/achat-user/achat-user.component';
import { AchatComponent } from './components/article/achat/achat.component';
import { ViewArticleComponent } from './components/article/view/view.component';
import { CreateArticleExampleComponent } from './components/articleExample/create/create.component';
import { ViewExampleComponent } from './components/articleExample/view/view.component';
import { CreateCategoryComponent } from './components/category/create/create.component';
import { ViewCategoryComponent } from './components/category/view/view.component';
import { EditPromessePriseComponent } from './components/edit-promesse-prise/edit-promesse-prise.component';
import { HomeComponent } from './components/home/home.component';
import { ListArticlesEmployeComponent } from './components/list-articles-employe/list-articles-employe.component';
import { ListArticlesComponent } from './components/list-articles/list-articles.component';
import { ListCategoryComponent } from './components/list-category/list-category.component';
import { ListExampleComponent } from './components/list-example/list-example.component';
import { ListPromesseUserComponent } from './components/list-promesse-user/list-promesse-user.component';
import { ListPromesseComponent } from './components/list-promesse/list-promesse.component';
import { ListePromessePriseComponent } from './components/liste-promesse-prise/liste-promesse-prise.component';
import { LoginEmployeComponent } from './components/login-employe/login-employe.component';
import { NotFoundComponent } from './components/partials/not-found/not-found.component';
import { ProduitMarchandComponent } from './components/produit-marchand/produit-marchand.component';
import { PromessePriseEnChargeComponent } from './components/promesse-prise-en-charge/promesse-prise-en-charge.component';
import { PromesseVueClientComponent } from './components/promesse-vue-client/promesse-vue-client.component';
import { CreatePromesseComponent } from './components/promesse/create/create.component';
import { EditPromesseComponent } from './components/promesse/edit/edit.component';
import { ViewPromesseComponent } from './components/promesse/view/view.component';
import { AccountPageComponent } from './components/users/account-page/account-page.component';
import { LoginComponent } from './components/users/login/login.component';
import { SignupComponent } from './components/users/signup/signup.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'login', component: LoginComponent},
  {path: 'accountPage', component: AccountPageComponent, canActivate: [AuthGuard]},
  {path: 'listPromesses', component: ListPromesseComponent, canActivate: [AuthGuard]},
  {path: 'createPromesse', component: CreatePromesseComponent, canActivate: [AuthGuard]},
  {path: 'priseEnCharge/:id', component: PromessePriseEnChargeComponent, canActivate: [AuthGuard]},
  {path: 'listPromessesCharge', component: ListePromessePriseComponent, canActivate: [AuthGuard]},
  {path: 'viewPromesse/:id', component: ViewPromesseComponent, canActivate: [AuthGuard]},
  {path: 'editPromesse/:id', component: EditPromesseComponent, canActivate: [AuthGuard]},
  {path: 'editPromesseP/:id', component: EditPromessePriseComponent, canActivate:[AuthGuard]},
  {path: 'editPromesseC/:id', component: PromesseVueClientComponent, canActivate: [AuthGuard]},
  {path: 'listArticles', component: ListArticlesComponent},
  {path: 'achatArticle', component: AchatComponent, canActivate: [AuthGuard]},
  {path: 'viewArticle/:id', component: ViewArticleComponent, canActivate: [AuthGuard]},
  {path: 'listArticleExample', component: ListExampleComponent, canActivate: [AuthGuard]},
  {path: 'createArticleExample', component: CreateArticleExampleComponent, canActivate: [AuthGuard]},
  {path: 'viewArticleExample/:id', component: ViewExampleComponent, canActivate: [AuthGuard]},
  {path: 'listCategory', component: ListCategoryComponent, canActivate: [AuthGuard]},
  {path: 'createCategory', component: CreateCategoryComponent, canActivate: [AuthGuard]},
  {path: 'viewCategory/:id', component: ViewCategoryComponent, canActivate: [AuthGuard]},
  {path: 'notFound', component: NotFoundComponent},
  {path: 'loginEm', component:LoginEmployeComponent},
  {path: 'listPromessesUser' , component:ListPromesseUserComponent},
  {path: 'produitVente', component: ProduitMarchandComponent, canActivate: [AuthGuard]},
  {path: 'achatUser', component: AchatUserComponent, canActivate: [AuthGuard]},
  {path: 'gestionArticles', component: ListArticlesEmployeComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
